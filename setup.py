#!/usr/bin/env python
# -*- coding: utf-8 -*-


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read().replace('.. :changelog:', '')

requirements = [
    # Flask
    'Flask==0.10.1',
    'Flask-Script',
    'MarkupSafe==0.23',
    'Werkzeug==0.10.4',
    'Jinja2==2.7.3',
    'itsdangerous==0.24',

    # Deployment
    'gunicorn>=19.1.1',

    # Misc Utilities
    'tzlocal==1.2',
]

setup(
    name='jiradog',
    version='0.1.0',
    description="A microservice that receives JIRA webhook payloads and publishes them to Datadog",
    long_description=readme + '\n\n' + history,
    author="Adam Petrovic",
    author_email='apetrovic@atlassian.com',
    url='https://bitbucket.org/atlassian/jiradog',
    packages=[
        'jiradog',
    ],
    package_dir={'jiradog':
                 'jiradog'},
    include_package_data=True,
    install_requires=requirements,
    license="ISCL",
    zip_safe=False,
    keywords='jiradog',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
)
