.. jiradog documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to JIRA to Datadog Event Publisher's documentation!
===========================================================

Contents:

.. toctree::
   :maxdepth: 2

   overview
   installation
   usage
   deployment
   testing
   authors
   history

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

