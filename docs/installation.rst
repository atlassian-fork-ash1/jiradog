============
Installation
============

Start by creating a virtual environment::

    mkvirtualenv jiradog

Update pip and install the required dependencies::

    pip install --upgrade pip
    pip install .

To install the required development dependencies::

    pip install -r requirements_dev.txt
